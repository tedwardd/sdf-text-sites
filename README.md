# Gopher and Gemini site files for sdf.org/elw

The files here populate the content for the following sites:

  * gopher://sdf.org/1/users/elw
  * gemini://gem.sdf.org/elw
  * gemini://sdf.org/elw
